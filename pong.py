from Tkinter import *
import math as math
import time
import random as random
import socket

class Ball(object):
	counter = 0 
	initV = 2.0
	newt = 0
	oldt = 0
	def __init__(self,canvas,x0 = 345,y0=200):
		self.collide = CollidePhysics()
		self.radius = 5.0
		self.ball_position = [x0,y0]
		self.v = [self.initV,-self.initV] # list contains x and y speed
		self.canVas = canvas
		self.actual =  (self.canVas.create_oval(self.ball_position[0]+0, 
						self.ball_position[1]+0, 2*self.radius+self.ball_position[0], 
						2*self.radius+self.ball_position[1], outline = 'white', fill = 'white', width = 1))
	def move(self):
		self.canVas.move(self.actual,self.v[0],self.v[1])

	def moveUnderGravity(self):
		self.canVas.move(self.actual,self.v[0],self.v[1])
		self.v[1]=self.v[1]+0.08
	def detec_collide_wall(self):
	# bounce ball when it hit the ceiling and floor
		if ((self.canVas.coords(self.actual)[1] <= 0) or (self.canVas.coords(self.actual)[1] >= 390)):
			self.v[1] = -self.v[1]
	def detec_collide_recpaddle(self,pad1coords,pad2coords,currentlevel,newspeedLevel):	
	# detect collision with rectangular paddle
		self.oldt = self.newt
		if self.collide.recpaddle_detec(self.canVas.coords(self.actual), pad1coords) or self.collide.recpaddle_detec(self.canVas.coords(self.actual), pad2coords):
			self.newt = time.time()
			tinterval = self.newt - self.oldt
			if (tinterval >= 1):
				self.v[0] = -self.v[0]
				if currentlevel<=4:			
					self.counter+=1
					if (self.counter==5):
						self.counter = 0 
						newspeedLevel.set(newspeedLevel.get()+0.5)
						
	def detec_collide_arcpaddle(self,pad1coords, pad2coords, ballRadius, padRadius,currentlevel,newspeedLevel):		
		self.oldt = self.newt
		ballcoords = self.canVas.coords(self.actual)
		if (self.collide.arcpaddle_detec(ballcoords,pad1coords,ballRadius, padRadius)):
			self.newt = time.time()
			tinterval = self.newt - self.oldt
			if (tinterval >= 1):
					px = (pad1coords[0]+pad1coords[2])*0.5 ; py = (pad1coords[1]+pad1coords[3])*0.5 
					bx = (ballcoords[0]+ballcoords[2])*0.5 ; by = (ballcoords[1]+ballcoords[3])*0.5
					n = [ bx-px,by-py ]
					n_moduli = math.sqrt(n[0]*n[0]+n[1]*n[1])
					 # normalizing vectors
					n[0] = n[0]/n_moduli ; n[1] = n[1]/n_moduli
					v_moduli = math.sqrt(self.v[0]*self.v[0]+self.v[1]*self.v[1])
					self.v[0] = n[0]*v_moduli ; self.v[1] = n[1]*v_moduli
					if currentlevel<=4:			
						self.counter+=1
						if (self.counter==5):
							self.counter = 0 
							newspeedLevel.set(newspeedLevel.get()+0.5)

		if (self.collide.arcpaddle_detec(self.canVas.coords(self.actual),pad2coords,ballRadius, padRadius)):
			self.newt = time.time()
			tinterval = self.newt - self.oldt
			if (tinterval >= 1):
					px = (pad2coords[0]+pad2coords[2])*0.5 ; py = (pad2coords[1]+pad2coords[3])*0.5 
					bx = (ballcoords[0]+ballcoords[2])*0.5 ; by = (ballcoords[1]+ballcoords[3])*0.5
					n = [ bx-px,by-py ]
					n_moduli = math.sqrt(n[0]*n[0]+n[1]*n[1])
					 # normalizing vectors
					n[0] = n[0]/n_moduli ; n[1] = n[1]/n_moduli
					v_moduli = math.sqrt(self.v[0]*self.v[0]+self.v[1]*self.v[1])
					self.v[0] = n[0]*v_moduli ; self.v[1] = n[1]*v_moduli
					if currentlevel<=4:			
						self.counter+=1
						if (self.counter==5):
							self.counter = 0 
							newspeedLevel.set(newspeedLevel.get()+0.5)

	def detec_score(self,ui):
		if (self.canVas.coords(self.actual)[0] <= 0):
			self.reset(ui)
			return 2
		if (self.canVas.coords(self.actual)[2] >= 700 ):
			self.reset(ui)
			return 1
	def reset(self,ui):
			ui.after(300)
			self.canVas.coords(self.actual,self.ball_position[0],self.ball_position[1],2*self.radius+self.ball_position[0],2*self.radius+self.ball_position[1])
			self.v = [self.initV,-self.initV]
			self.counter = 0
			
class Paddle(object):
	def __init__(self,canvas,up, down, x0 = 5, y0 =4):
		self.pad_position = [x0,y0]
		self.width = 10
		self.height = 50
		self.speed = 5
		self.up_key = up 
		self.down_key = down 
		self.canVas = canvas
		self.RecPad()
	def RecPad(self):
		self.actual =  (self.canVas.create_rectangle(0+self.pad_position[0], 0+self.pad_position[1], 
						self.width+self.pad_position[0], 
						self.height+self.pad_position[1], outline="white", fill="white"))	
	def reCreateRecPad(self):		
		coords = self.canVas.coords(self.actual)
		if coords[0]<350:
			self.canVas.delete(self.actual)
			centerx = (coords[0]+coords[2])*0.5 
			coords = centerx, coords[1],centerx+self.width,coords[3]
			self.actual = self.canVas.create_rectangle(coords, outline="white",fill="white")
		else:
			self.canVas.delete(self.actual)
			centerx = (coords[0]+coords[2])*0.5 
			coords = centerx-self.width, coords[1],centerx,coords[3]
			self.actual = self.canVas.create_rectangle(coords, outline="white",fill="white")
	def ArcPad(self):
		coords = self.canVas.coords(self.actual)
		if coords[0]<350:
			self.canVas.delete(self.actual)
			coords = coords[0]-self.height/2, coords[1],coords[0]+self.height/2,coords[3] 
			self.actual = (self.canVas.create_arc(coords, start = -90, extent=180, outline="white", fill="white"))
		else:
			self.canVas.delete(self.actual)
			coords = coords[2]+self.height/2, coords[1],coords[2]-self.height/2,coords[3] 
			self.actual = (self.canVas.create_arc(coords, start = 90, extent=180, outline="white", fill="white"))
	def move(self, pressed):
		if self.up_key in pressed and self.down_key not in pressed:
			if self.canVas.coords(self.actual)[1]>=10:
							self.canVas.move(self.actual,0,-self.speed)
		elif self.down_key in pressed and self.up_key not in pressed:
	 		if self.canVas.coords(self.actual)[3]<=400-8:	
	 			self.canVas.move(self.actual,0,self.speed)

class Portals(object):
	start = 0 ; life = 0; 
	def __init__(self, canvas):
		self.collide = CollidePhysics()
		self.canvasInPortalClass = canvas
		self.createPortals()

	def createPortals(self):
		self.portal1pos = [random.randint(200,500), 4] #coords for top left corner of portal 1
		self.portal2pos = [random.randint(200,500), 395]
		self.portalLength = 100
		self.portalThickness = 5
		self.radius = 5 # radius of the ball, needs to be changed when ball diameter is changed
		self.portal1 = (self.canvasInPortalClass.create_oval(self.portal1pos[0], self.portal1pos[1], self.portal1pos[0]+self.portalLength, 
						self.portal1pos[1]+self.portalThickness, outline = 'purple', fill = 'purple'))
		self.portal2 = (self.canvasInPortalClass.create_oval(self.portal2pos[0], self.portal2pos[1], self.portal2pos[0]+self.portalLength, 
						self.portal2pos[1]+self.portalThickness, outline = 'purple', fill = 'purple'))
		self.setLife()

	def timeCheck(self):
		if ( abs(time.time() - (self.start + self.life))<0.5 ):
			self.destroyPortals()
			self.createPortals()

	def teleport(self, actualBall, coords1, coords2, coords3): # coords1 = ball_position
		self.ball = actualBall
		self.ball_coords = coords1
		self.portal1_coords = coords2
		self.portal2_coords = coords3
		# expand area of detection
		if self.collide.recpaddle_detec(self.ball_coords,self.portal1_coords):
			self.canvasInPortalClass.coords(self.ball, self.portal2_coords[0]+self.portalLength/2-2*self.radius, self.portal2_coords[1]-2*self.radius, self.portal2_coords[2]-self.portalLength/2, self.portal2_coords[3]-self.portalThickness)
		elif self.collide.recpaddle_detec(self.ball_coords,self.portal2_coords):
			self.canvasInPortalClass.coords(self.ball, self.portal1_coords[0]+self.portalLength/2-2*self.radius, self.portal1_coords[1]+self.portalThickness, self.portal1_coords[2]-self.portalLength/2, self.portal1_coords[3]+2*self.radius)
	
	def destroyPortals(self):
		self.canvasInPortalClass.delete(self.portal1)
		self.canvasInPortalClass.delete(self.portal2)
	def setLife(self):
		self.life = random.randint(4,6)
		self.start = time.time()

class Obstacle(object):

	oldt1 = 0 ; newt1 = 0 ; oldt2 = 0 ; newt2 = 0; cv = [2,2]
	def __init__(self,Type,canvas):

		self.canvas = canvas 
		self.collideDetector = CollidePhysics()
		if Type == 'rectangle':
			self.create_recObs()
		if Type == 'oval':
			self.create_ovalObs()
		if Type == 'monster':
			self.create_triMonster()
		if Type == 'creature':
			self.create_Creature([random.randint(200,500), random.randint(50,350)])
		if Type == 'cloud':
			self.create_Cloud()
		self.type = Type

	def create_recObs(self):
		initPos = [random.randint(200,500), random.randint(50,350)]
		self.width = random.randint(10,30) ; self.height = random.randint(60,80);
		self.actual = self.canvas.create_rectangle(initPos[0],initPos[1],initPos[0]+self.width,initPos[1]+self.height,outline = 'blue', fill = 'blue')
		self.setLife()
	def create_ovalObs(self):
		initPos = [random.randint(200,500), random.randint(50,350)]
		self.radii = random.randint(30,50);
		self.actual = self.canvas.create_oval(initPos[0],initPos[1],initPos[0]+self.radii,initPos[1]+self.radii,outline = 'orange', fill = 'orange')
		self.setLife()

	def create_Creature(self, coords):
		self.creatureCoords = coords # contains x and y coordinates
		self.im = PhotoImage(file = "original.gif")
		self.tk_im = self.canvas.create_image(self.creatureCoords[0],self.creatureCoords[1],image = self.im)
		self.actual = (self.canvas.create_oval(self.canvas.coords(self.tk_im)[0]-30, self.canvas.coords(self.tk_im)[1]-30, 
			self.canvas.coords(self.tk_im)[0]+30, self.canvas.coords(self.tk_im)[1]+30, 
			outline = ''))
	def creature_move(self):
		
		coords = self.canvas.coords(self.actual)
		self.canvas.delete(self.tk_im)
		self.canvas.move(self.actual,self.cv[0],self.cv[1])
		self.tk_im = self.canvas.create_image( (coords[0]+coords[2])*0.5,(coords[1]+coords[3])*0.5,image = self.im )
		self.oldt1 = self.newt1
		self.oldt2 = self.newt2
		if coords[0]<=0  or coords[2]>=700:
			self.newt1 = time.time()
			tinterval = self.newt1 - self.oldt1
			if tinterval>0.05:
				self.cv[0] = -self.cv[0]
		if (coords[1]<=0)  or (coords[3]>=400):
			self.newt2 = time.time()
			tinterval = self.newt2 - self.oldt2
			if tinterval>0.05:
				self.cv[1] = -self.cv[1]

	def create_Cloud(self):
		self.im = PhotoImage(file = "cloud1.gif")
		self.tk_im = self.canvas.create_image(350,200,image=self.im)
		self.cloud_detec_length = 180
		self.cloud_detec_width = 80
		self.actual = (self.canvas.create_oval(self.canvas.coords(self.tk_im)[0]-90, self.canvas.coords(self.tk_im)[1]-40, 
			self.canvas.coords(self.tk_im)[0]+self.cloud_detec_length-90, self.canvas.coords(self.tk_im)[1]+self.cloud_detec_width-40, 
			outline = ''))

	def deleteObs(self):
		self.canvas.delete(self.actual)

	def deleteCreature(self):
		self.canvas.delete(self.creature_tk_im)
		self.canvas.delete(self.creature_actual)

	def checkTimer(self):
		if ( abs(time.time() - (self.start + self.life))<0.5 ):
			self.deleteObs()
			if self.type == 'rectangle':
				self.create_recObs()
			if self.type == 'oval':
				self.create_ovalObs()
	def setLife(self):
		self.life = random.randint(4,7)
		self.start = time.time()
	def detec_collision(self,ballcoords,ballV):		

			if self.type == 'rectangle':		
				if self.collideDetector.recpaddle_detec(ballcoords, self.canvas.coords(self.actual)):
					ballcenterx = 0.5*(ballcoords[0]+ballcoords[2]) ;
					ballcentery = 0.5*(ballcoords[1]+ballcoords[3]) ;
					if (ballcenterx < self.canvas.coords(self.actual)[0] ) or (ballcenterx > self.canvas.coords(self.actual)[2]):
						ballV[0] = -ballV[0] ;
					if (ballcentery < self.canvas.coords(self.actual)[1] ) or (ballcentery > self.canvas.coords(self.actual)[3]):
						ballV[1] = -ballV[1] ;
					self.deleteObs()
					self.create_recObs()
			if self.type == 'oval':
				if self.collideDetector.arcpaddle_detec(ballcoords, self.canvas.coords(self.actual),5, (self.canvas.coords(self.actual)[2]-self.canvas.coords(self.actual)[0])/2.5):
					obscoords = self.canvas.coords(self.actual) ;
					obsx = (obscoords[0]+obscoords[2])*0.5 ; obsy = (obscoords[1]+obscoords[3])*0.5 
					bx = (ballcoords[0]+ballcoords[2])*0.5 ; by = (ballcoords[1]+ballcoords[3])*0.5
					n = [ bx-obsx,by-obsy ]
					n_moduli = math.sqrt(n[0]*n[0]+n[1]*n[1])
					n[0] = n[0]/n_moduli ; n[1] = n[1]/n_moduli
					v_moduli = math.sqrt(ballV[0]*ballV[0]+ballV[1]*ballV[1])
					ballV[0] = n[0]*v_moduli ; ballV[1] = n[1]*v_moduli
					self.deleteObs()
					self.create_ovalObs()
			if self.type == 'cloud':
				self.oldt1 = self.newt1
				if self.collideDetector.cloud_detec(ballcoords,self.canvas.coords(self.actual)):
					self.newt1 = time.time()
					tinterval = self.newt1 - self.oldt1
					if (tinterval >= 1):
						modulli=math.sqrt(ballV[0]*ballV[0]+ballV[1]*ballV[1])
						theta = math.radians(random.random()*360)
						ballV[0]=modulli*math.cos(theta) 
						ballV[1]=modulli*math.sin(theta)
			if self.type == 'creature':
				if self.collideDetector.arcpaddle_detec(ballcoords, self.canvas.coords(self.actual),5, (self.canvas.coords(self.actual)[2]-self.canvas.coords(self.actual)[0])/2.5):
					obscoords = self.canvas.coords(self.actual) 
					obsx = (obscoords[0]+obscoords[2])*0.5 ; obsy = (obscoords[1]+obscoords[3])*0.5 
					bx = (ballcoords[0]+ballcoords[2])*0.5 ; by = (ballcoords[1]+ballcoords[3])*0.5
					n = [ bx-obsx,by-obsy ]
					n_moduli = math.sqrt(n[0]*n[0]+n[1]*n[1])
					n[0] = n[0]/n_moduli ; n[1] = n[1]/n_moduli
					v_moduli = math.sqrt(ballV[0]*ballV[0]+ballV[1]*ballV[1])
					ballV[0] = n[0]*v_moduli ; ballV[1] = n[1]*v_moduli

class CollidePhysics(object):
	def recpaddle_detec(self, coords1, coords2): # coords1 = ball_coords, coords2 = paddle_coords
		return (coords1[2] >= coords2[0] and coords1[3] >= coords2[1] and coords1[0] <= coords2[2] and coords1[1] <= coords2[3])		
	def arcpaddle_detec(self,ballCoords,padCoords,ballRadius, padRadius):
		padCenterx = (padCoords[0]+padCoords[2])*0.5;
		padCentery = (padCoords[1]+padCoords[3])*0.5;
		ballCenterx = (ballCoords[0]+ballCoords[2])*0.5;
		ballCentery = (ballCoords[1]+ballCoords[3])*0.5;
		distance = math.sqrt(pow(padCenterx-ballCenterx,2)+pow(padCentery-ballCentery,2));
		if abs(distance-(ballRadius+padRadius))<=5:
			return True;
	def cloud_detec(self, ballCoords, cloudcoords):

		ballcenterx = (ballCoords[0]+ballCoords[2])*0.5; cloudcenterx = (cloudcoords[0]+cloudcoords[2])*0.5
		ballcentery = (ballCoords[1]+ballCoords[3])*0.5; cloudcentery = (cloudcoords[1]+cloudcoords[3])*0.5
		a = (cloudcoords[2]-cloudcoords[0]) * 0.5 
		b = (cloudcoords[3]-cloudcoords[1]) * 0.5
		if ( pow(ballcenterx-cloudcenterx,2)/pow(a,2)+pow(ballcentery-cloudcentery,2)/pow(b,2)-1 < -0 ):
			return True
class UI(Frame):
	winHeight = 400
	winWidth = 700
	p1scores = 0 
	p2scores = 0
	currentlevel = 1
	gravity=0
	obstaclesOn = 0
	portalsOn = 0
	cloudOn = 0
	creatureOn = 0
	obs1 = 0
	obs2 = 0
	obs3 = 0
	obs4 = 0
	def __init__(self,parent):
		Frame.__init__(self,parent)
		self.root = parent	
		self.collide = CollidePhysics()
		self.root.title("PONG")
		self.canvas = Canvas(self.root,bg='black')
		self.canvas.configure(width = self.winWidth, height=self.winHeight)
		self.canvas.create_line(self.winWidth/2,0,self.winWidth/2,self.winHeight,fill = 'yellow')
		self.canvas.grid(row=0, column = 0,columnspan=10)
		self.label = self.canvas.create_text(self.winWidth/2,self.winHeight-20, text=str(self.p1scores)+" | "+str(self.p2scores), fill='white')
		self.ball = Ball(self.canvas)
		self.pad1 = Paddle(self.canvas,'w','s')
		self.pad2 = Paddle(self.canvas,'Up','Down',686)
		Button(self.root,text="START", command=self.start).grid(row=1,column=0)
		Button(self.root,text="QUIT", command=self.root.destroy).grid(row=2,column=0)
		Button(self.root,text="RESET", command=self.restart).grid(row=3,column=0)
		# ball speed adjustment
		Label(self.root, text="Ball Speed Control").grid(row=1,column=1)
		self.speedLevel = Scale(self.root, from_=1, to=4,orient = HORIZONTAL,resolution=0.5,command = self.paceChange)
		self.speedLevel.grid(row=2,column=1)
		self.speedLevel.set(1)
		# pad speed adjustment
		Label(self.root, text="Paddle Speed Control").grid(row=1,column=2)
		self.padSpeedLevel = Scale(self.root, from_=5, to=15,orient = HORIZONTAL,resolution=1,command = self.padSpeedChange).grid(row=2,column=2)
		self.pad_shape = IntVar()
		self.PaddleShapeCheckBox = Checkbutton(self.root, text="Tricky Paddle", variable=self.pad_shape, command=self.changePaddleShape).grid(row=3,column=2)
		self.gravitySwitch = IntVar()
		self.gravitySwitchCheckBox = Checkbutton(self.root, text="Gravity On", variable=self.gravitySwitch).grid(row=1,column=3)
		self.portalSwitch = IntVar()
		self.portalSwitchCheckBox = Checkbutton(self.root, text = 'Portals On', variable = self.portalSwitch).grid(row = 2, column = 3)
		self.obstacleSwitch = IntVar()
		self.obstacleSwitchCheckBox = Checkbutton(self.root, text="Add Obstacles", variable=self.obstacleSwitch).grid(row=3, column = 3)
		self.creatureSwitch = IntVar()
		self.creatureSwitchCheckBox = Checkbutton(self.root, text="Creature", variable=self.creatureSwitch).grid(row=2,column=4)
		self.cloudSwitch = IntVar()
		self.cloudSwitchCheckBox = Checkbutton(self.root, text="Cloud", variable=self.cloudSwitch).grid(row=1,column=4)

		self.buttons = set()
		self.root.bind_all('<KeyPress>', lambda event: self.buttons.add(event.keysym))
		self.root.bind_all('<KeyRelease>', lambda event: self.buttons.discard(event.keysym))

	def start(self):
		self.after(30, self.update)
	def restart(self):
		self.after(300)
		self.ball.reset(self)
		self.p1scores=0
		self.p2scores=0
		self.reset_panel()
	def reset_panel(self):
		self.canvas.delete(self.label)
		self.label = self.canvas.create_text(350,380, text=str(self.p1scores)+" | "+str(self.p2scores), fill='white')
		self.canvas.delete(self.speedLevel)			
		self.speedLevel = Scale(self.root, from_=1, to=4,orient = HORIZONTAL,resolution=0.5,command = self.paceChange)
		self.speedLevel.grid(row=2,column=1)
		self.currentlevel = 1
	def update(self):

		if self.portalSwitch.get()==1:
			if self.portalsOn == 0:
				self.portals = Portals(self.canvas)
				self.portalsOn = 1 
			if self.portalsOn == 1:
				self.portals.timeCheck() 
				self.portals.teleport(self.ball.actual, self.canvas.coords(self.ball.actual), self.canvas.coords(self.portals.portal1), 
		 		self.canvas.coords(self.portals.portal2))
		if self.portalSwitch.get() == 0:
		 	if self.portalsOn == 1:
		 		self.canvas.delete(self.portals.portal1)
		 		self.canvas.delete(self.portals.portal2)
		 		self.portalsOn = 0		 		
		if self.obstacleSwitch.get()==1:
			if self.obstaclesOn == 0:			
				self.showObstacles()
				self.obstaclesOn = 1 ;
			if self.obstaclesOn == 1:
				self.obs1.checkTimer() ; self.obs2.checkTimer()
				self.obs3.checkTimer() ; self.obs3.checkTimer()
				self.obs1.detec_collision(self.canvas.coords(self.ball.actual), self.ball.v) ; 
				self.obs2.detec_collision(self.canvas.coords(self.ball.actual), self.ball.v) ;
				self.obs3.detec_collision(self.canvas.coords(self.ball.actual), self.ball.v) ;
				self.obs4.detec_collision(self.canvas.coords(self.ball.actual), self.ball.v) ;
		elif self.obstacleSwitch.get( )==0:		
			if self.obstaclesOn == 1:
				self.obstaclesOn = 0 
			if hasattr(self.obs1, 'actual'):
				self.canvas.delete(self.obs1.actual)
			if hasattr(self.obs2, 'actual'):
				self.canvas.delete(self.obs2.actual)
			if hasattr(self.obs3, 'actual'):
				self.canvas.delete(self.obs3.actual)
			if hasattr(self.obs4, 'actual'):
				self.canvas.delete(self.obs4.actual)
		if self.gravitySwitch.get( )==0:
			self.ball.move( )
		elif self.gravitySwitch.get( )==1:
			self.ball.moveUnderGravity()
		if self.cloudSwitch.get( )==1:
			if self.cloudOn ==0:
				self.showCloud( )
				self.cloudOn =1
			elif self.cloudOn == 1:
				self.cloud.detec_collision( self.canvas.coords(self.ball.actual), self.ball.v)
		elif self.cloudSwitch.get( )==0:
			if self.cloudOn ==1 :
				self.canvas.delete(self.cloud.actual)
				self.canvas.delete(self.cloud.tk_im)
				self.cloudOn = 0
		if self.creatureSwitch.get( )==1:
			if self.creatureOn ==0:
				self.showCreature( )
				self.creatureOn =1
			elif self.creatureOn == 1:
				self.creature.creature_move()
				self.creature.detec_collision( self.canvas.coords(self.ball.actual), self.ball.v)
		elif self.creatureSwitch.get( )==0:
			if self.creatureOn ==1 :
				self.canvas.delete(self.creature.actual)
				self.canvas.delete(self.creature.tk_im)
				self.creatureOn = 0
		self.ball.detec_collide_wall()

		if self.pad_shape.get() == 0:
			self.ball.detec_collide_recpaddle(self.canvas.coords(self.pad1.actual),self.canvas.coords(self.pad2.actual),self.currentlevel,self.speedLevel)
		if self.pad_shape.get() == 1:
			self.ball.detec_collide_arcpaddle(self.canvas.coords(self.pad1.actual),self.canvas.coords(self.pad2.actual),self.ball.radius,self.pad1.height/2.4,self.currentlevel,self.speedLevel)
		switch1 =self.ball.detec_score(self)
		if switch1==1:
			self.p1scores+=1
			self.reset_panel()
		if switch1==2:
			self.p2scores+=1
			self.reset_panel()

		self.pad1.move(self.buttons)
		self.pad2.move(self.buttons)		
		self.after(10, self.update)
	def paceChange(self,new):
		new = float(new)
		self.ball.v[0]= self.ball.v[0]/self.currentlevel*new
		self.ball.v[1]= self.ball.v[1]/self.currentlevel*new
		self.currentlevel = new
	def changePaddleShape(self):
		if self.pad_shape.get()==0:
			self.pad1.reCreateRecPad()
			self.pad2.reCreateRecPad()
		if self.pad_shape.get()==1:
			self.pad1.ArcPad()
			self.pad2.ArcPad()
	def padSpeedChange(self,newSpeed):
		self.pad1.speed = int(newSpeed)
		self.pad2.speed = int(newSpeed)
	def turnOnPortals(self):
		self.portals = Portals(self.canvas)
		self.now = time.time()
		self.duration = random.randint(20,30)
		self.endTime = self.now+self.duration
		if self.portalSwitch.get() == 0:
			self.portals.destroy()
	def showObstacles(self):
		self.obs1 = Obstacle('rectangle',self.canvas)
		self.obs2 = Obstacle('rectangle',self.canvas)
		self.obs3 = Obstacle('oval',self.canvas)
		self.obs4 = Obstacle('oval',self.canvas)
	def showMonster(self):
		self.monster = Obstacle('monster',self.canvas)
	def showCreature(self):
		self.creature = Obstacle('creature', self.canvas)
	def showCloud(self):
		self.cloud = Obstacle('cloud', self.canvas)

def main():
    root = Tk()
    ui = UI(root)
    root.geometry("700x550")
    root.mainloop()  
if __name__ == '__main__':
    main()